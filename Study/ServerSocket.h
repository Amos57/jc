#pragma once
#include "Socket.h"

namespace java {
	namespace net {
class ServerSocket : public Object
{
		
	SOCKET sock;
	SOCKADDR_IN sa;
	int sizeStruct;
public:
	ServerSocket(const int& port);

	Socket accept_();
	void close();
};
	
	}
}



