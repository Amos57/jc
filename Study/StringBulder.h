#pragma once
#include "CharSequence.h"
#include "System.h"
#include "Integer.h"

import java::lang::CharSequence;
import java::lang::Integer;
namespace java {
	namespace lang {
	class StringBulder 
	{
	char* data;
	int size;
	int capast;
	public:
		StringBulder();
		StringBulder(const int&);
		int length();
		int capacity();
		char charAt(const int& index);
		//CharSequence& subSequence(const int& start, const  int& end);


		/**
        * Ensures that the capacity is at least equal to the specified minimum.
        * If the current capacity is less than the argument, then a new internal
        * array is allocated with greater capacity. The new capacity is the
        * larger of:
        * <ul>
        * <li>The <code>minimumCapacity</code> argument.
        * <li>Twice the old capacity, plus <code>2</code>.
        * </ul>
        * If the <code>minimumCapacity</code> argument is nonpositive, this
        * method takes no action and simply returns.
        *
        * @param   minimumCapacity   the minimum desired capacity.
        */
	   public: void ensureCapacity(const int &minimumCapacity);

		/**
		 * This method has the same contract as ensureCapacity, but is
		 * never synchronized.
		 */
	   private: void ensureCapacityInternal(const int& minimumCapacity);

		/**
		 * This implements the expansion semantics of ensureCapacity with no
		 * size check or synchronization.
		 */
	   public:void expandCapacity(int minimumCapacity);


		/**
        * Attempts to reduce storage used for the character sequence.
        * If the buffer is larger than necessary to hold its current sequence of
        * characters, then it may be resized to become more space efficient.
        * Calling this method may, but is not required to, affect the value
        * returned by a subsequent call to the {@link #capacity()} method.
        */
		void trimToSize();



		/**
		 * Returns the index within this string of the rightmost occurrence
		 * of the specified substring.  The rightmost empty string "" is
		 * considered to occur at the index value <code>this.length()</code>.
		 * The returned index is the largest value <i>k</i> such that
		 * <blockquote><pre>
		 * this.toString().startsWith(str, k)
		 * </pre></blockquote>
		 * is true.
		 *
		 * @param   str   the substring to search for.
		 * @return  if the string argument occurs one or more times as a substring
		 *          within this object, then the index of the first character of
		 *          the last such substring is returned. If it does not occur as
		 *          a substring, <code>-1</code> is returned.
		 * @throws  java::lang::NullPointerException  if <code>str</code> is
		 *          <code>null</code>.
		 */
		int lastIndexOf(const String& str);

		/**
        * Returns the index within this string of the last occurrence of the
        * specified substring. The integer returned is the largest value <i>k</i>
        * such that:
        * <blockquote><pre>
        *     k <= Math.min(fromIndex, str.length()) &&
        *                   this.toString().startsWith(str, k)
        * </pre></blockquote>
        * If no such value of <i>k</i> exists, then -1 is returned.
        *
        * @param   str         the substring to search for.
        * @param   fromIndex   the index to start the search from.
        * @return  the index within this sequence of the last occurrence of the
        *          specified substring.
        * @throws  java.lang.NullPointerException if <code>str</code> is
        *          <code>null</code>.
        */
		int lastIndexOf(const String& str,const int fromIndex);

		/**
        * Causes this character sequence to be replaced by the reverse of
        * the sequence. If there are any surrogate pairs included in the
        * sequence, these are treated as single characters for the
        * reverse operation. Thus, the order of the high-low surrogates
        * is never reversed.
        *
        * Let <i>n</i> be the character length of this character sequence
        * (not the length in <code>char</code> values) just prior to
        * execution of the <code>reverse</code> method. Then the
        * character at index <i>k</i> in the new character sequence is
        * equal to the character at index <i>n-k-1</i> in the old
        * character sequence.
        *
        * <p>Note that the reverse operation may result in producing
        * surrogate pairs that were unpaired low-surrogates and
        * high-surrogates before the operation. For example, reversing
        * "&#92;uDC00&#92;uD800" produces "&#92;uD800&#92;uDC00" which is
        * a valid surrogate pair.
        *
        * @return  a reference to this object.
        */
		StringBulder reverse();



		StringBulder append(const char&);
		StringBulder append(const Object&);
		StringBulder append(const Integer&);
		StringBulder append(const int&);
		StringBulder append(const long&);
		StringBulder append(const float&);
		StringBulder append(const double&);
		StringBulder append(const jchar&);
		StringBulder append(const String&);

		StringBulder delete_(const int& st,const int & end);
		StringBulder delete_(const int& in);
		StringBulder clear();

		~StringBulder() {
				delete[] data;
		}

	};
	}
}
