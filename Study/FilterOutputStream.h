#pragma once
#include "IOStreamAbst.h"
#include "Throwable.h"

import java::lang::IndexOutOfBoundsException;

namespace java {
	namespace io {


class FilterOutputStream extends OutputStream{


protected: OutputStream* out;
public:
	FilterOutputStream(OutputStream* out) {
		this->out = out;
	}
		 void flush(){
			 out->flush();
		 }
		 void write(ByteArray & b, int off, int len)  {
			 if ((off | len | (b.size() - (len + off)) | (off + len)) < 0)
				 throw  IndexOutOfBoundsException();

			 for (int i = 0; i < len; i++) {
				 write(b[off + i]);
			 }
		 }


		 void write(ByteArray& b) {
			 write(b, 0, b.size());
		 }

		 void write(const int& b) {
			 out->write(b);
		 }
		 void close() {
				 flush();
				 out->close();
			 }
};

	}
}