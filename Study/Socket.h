#pragma once
#undef UNICODE
#define UNICODE
#undef _WINSOCKAPI_
#define _WINSOCKAPI_
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "String.h"
#include "IOStreamAbst.h"

#include <WinSock2.h>
#include <iostream>

#pragma comment(lib, "Ws2_32.lib")
#pragma warning(disable :4996)




using java::lang::Object;
using java::lang::String;
using java::io::InputStream;
using java::io::OutputStream;

namespace java {
	namespace net {

#define SOCKET_DISCONNECTED -2

class Socket : public Object
{
private:
 SOCKET socke;
 bool isConnect = false;
 bool closing   = false;

 class SocketInputStream : public InputStream {

	private:
		SOCKET* sock = nullptr;
		bool* connected;
		bool eof = false;
		bool* closing;


	public:
		SocketInputStream(SOCKET* socket, bool* coon,bool*cl)
			: sock(socket), connected(coon),closing(cl) {}

		int read() {
			if (eof) {
				return -1;
			}
			int os = 0;
			int l = 1;
			ByteArray ba(1);
			int n = read(ba, os, l);
			if (n <= 0) {
				return -1;
			}
			return ba[0] & 0xff;
		}

		int socketRead(SOCKET * s,ByteArray& b,int& off,int& length,int& timeout) {
			
			int res=recv(*s,&b[off],length,NULL);
			if (res == 0)
				throw std::exception("onnection closed");

			if (res == SOCKET_ERROR)
				throw std::exception("error read bytes");
			return res;
		}

		int read(ByteArray& b, int& off, int& length, int& timeout) {
			int n;

			// EOF already encountered
			if (eof) {
				return -1;
			}

			// connection reset
			//if (impl.isConnectionReset()) {
			//	throw new SocketException("Connection reset");
			//}

			// bounds check
			if (length <= 0 || off < 0 || length > b.size() - off) {
				if (length == 0) {
					return 0;
				}
				throw std::exception("error lenght");//ERROR
			}

			bool gotReset = false;

			// acquire file descriptor and do the read

			try {
				n = socketRead(sock, b, off, length, timeout);
				if (n > 0) {
					return n;
				}
			}
			catch (...) {
				gotReset = true;
			}

			/*
			 * We receive a "connection reset" but there may be bytes still
			 * buffered on the socket
			 */
			if (gotReset) {
				//impl.setConnectionResetPending();
				//impl.acquireFD();
				try {
					n = socketRead(sock, b, off, length, timeout);
					if (n > 0) {
						return n;
					}
				}
				catch (...) {
				}

			}
		}

			int read(ByteArray & b, int& off, int& length) {
				int to = 3000;
				return read(b, off, length,to );
			}
			long skip(long& numbytes) {
				if (numbytes <= 0) {
					return 0;
				}
				long n = numbytes;
				int buflen = min(1024, n);
				int len = min((long)buflen, n);
				ByteArray data(buflen);
				int st = 0;
				while (n > 0) {
					int r = read(data, st,len );
					if (r < 0) {
						break;
					}
					n -= r;
				}
				return numbytes - n;
			}
			bool cl = false;
			void close() {
				// Prevent recursion. See BugId 4484411
				if (cl)
					return;
				cl = true;
				if (sock) {
					if (!closing)
						closesocket(*sock);
					*sock = INVALID_SOCKET;
					*closing = true;
				}
				else

					cl = false;
			}
		} sockInputStream;
 class SocketOutputStraem : public OutputStream {
		private:
			SOCKET* socket;
			bool* isConnected;
			bool *closing;
			ByteArray temp;

		public:
			SocketOutputStraem(SOCKET* socket,bool *conn,bool * closing) :
				socket(socket),isConnected(conn), closing(closing){}
			void write(const int& b) {
				temp[0] =(char)(b);
				socketWrite(temp, 0, 1);
			}
			void socketWrite(ByteArray& b,const int& off,const int& len){


				if (len <= 0 || off < 0 || len > b.size() - off) {
					if (len == 0) {
						return;
					}
					throw std::exception("len == ");
				}

				try {
					socketWrite0(socket, b, off, len);
				}
				catch (...) {

				}
			}
			void write(ByteArray &b ) {
				socketWrite(b, 0, b.size());
			}

			/**
			 * Writes <i>length</i> bytes from buffer <i>b</i> starting at
			 * offset <i>len</i>.
			 * @param b the data to be written
			 * @param off the start offset in the data
			 * @param len the number of bytes that are written
			 * @exception SocketException If an I/O error has occurred.
			 */
			 void write(ByteArray & ba, int& off, int& len){
				socketWrite(ba, off, len);
			}

			/**
			 * Closes the stream.
			 */
			 bool cl = false;
			 void close() {
				// Prevent recursion. See BugId 4484411
				if (cl)
					return;
				cl = true;
				if (socket) {
					if (!closing)
						closesocket(*socket);
					    *socket = INVALID_SOCKET;
						*closing = true;
				}
				else

				cl = false;
			}
	private:
		void socketWrite0(SOCKET * sock, ByteArray & ba,const int& off,
			const int& len) {
			int i=send(*sock,&ba[off],len,NULL);
			if (i == SOCKET_ERROR)
				throw std::exception("error ");
		}

	}sockOutpStraem;


public: Socket();
public: Socket(const String&host,const int&port);
	  friend class ServerSocket;
	  SocketInputStream getInputStream();
	  SocketOutputStraem getOutputStream();
	  bool isConnected();
	  bool isClosed();
	  void close();
	  bool disconnect();
	  
	  ~Socket();

};
	}
}

