#include "FileDescriptor.h"


using java::io::FileDescriptor;

const FileDescriptor FileDescriptor::err = standardStream(2);
const FileDescriptor FileDescriptor::in  = standardStream(0);
const FileDescriptor FileDescriptor::out = standardStream(1);

FileDescriptor::FileDescriptor(): fd(0) {}
FileDescriptor::FileDescriptor(const int &fb) : fd(fb){

if (fb == 0) { 
	handle =    GetStdHandle(STD_INPUT_HANDLE);
} else if (fb == 1) {
	handle = GetStdHandle(STD_OUTPUT_HANDLE);
} else if (fb == 2) {
	handle = GetStdHandle(STD_ERROR_HANDLE);
} 
handle =reinterpret_cast<HANDLE>(-1);
}


HANDLE FileDescriptor::set(const int& val) {
	if (val == 0) {
		return GetStdHandle(STD_INPUT_HANDLE); 
	}
	else if (val == 1) {
		return GetStdHandle(STD_OUTPUT_HANDLE); 
	}
	else if (val == 2) {
		return GetStdHandle(STD_ERROR_HANDLE); 
	}
	else {
		return reinterpret_cast<HANDLE>(-1);
	}
}


 FileDescriptor FileDescriptor::standardStream(int fd) {
	FileDescriptor desc;
	desc.fd = fd;
	desc.handle = set(fd);
	return desc;
}


 bool FileDescriptor::valid() {
	 return ((handle != reinterpret_cast<HANDLE>(-1)) || (fd != -1));
 }

