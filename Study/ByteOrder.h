#pragma once
#include "String.h"

import java::lang::Object;
import java::lang::String;

namespace java {
	namespace nio {
	
		class ByteOrder extends Object{

		private:
			String name;
			ByteOrder(const String& name): name(name) {}
			
		public:
			static final ByteOrder BIG_ENDIAN;


			 /**
	 * Constant denoting little-endian byte order.  In this order, the bytes of
	 * a multibyte value are ordered from least significant to most
	 * significant.
	 */
			 static final ByteOrder LITTLE_ENDIAN;


			/**
			 * Retrieves the native byte order of the underlying platform.
			 *
			 * <p> This method is defined so that performance-sensitive Java code can
			 * allocate direct buffers with the same byte order as the hardware.
			 * Native code libraries are often more efficient when such buffers are
			 * used.  </p>
			 *
			 * @return  The native byte order of the hardware upon which this Java
			 *          virtual machine is running
			 */
			 static ByteOrder nativeOrder() {
				 return ByteOrder("");// Bits::byteOrder();
			}

			/**
			 * Constructs a string describing this object.
			 *
			 * <p> This method returns the string <tt>"BIG_ENDIAN"</tt> for {@link
			 * #BIG_ENDIAN} and <tt>"LITTLE_ENDIAN"</tt> for {@link #LITTLE_ENDIAN}.
			 * </p>
			 *
			 * @return  The specified string
			 */
			 std::string toString() {
				return name.toStdString();
			}

		};

	}
}
