#pragma once
#include "Throwable.h"
#include "CharBuffer.h"
#include <mutex>
#include <vector>
#include "Math.h"



import java::lang::NullPointerException;
import java::lang::Object;
import java::lang::IllegalArgumentException;
import java::lang::Math;

import java::io::IOException;
import java::nio::CharBuffer;




typedef std::vector<jchar> ByteArray;

namespace java {

	namespace io {
	
		abstract class Reader extends Object {
protected:
	std::mutex lock;



	/**
	 * Creates a new character-stream reader whose critical sections will
	 * synchronize on the reader itself.
	 */
	Reader() {
		//lock = this;
	}
	/**
 * Creates a new character-stream reader whose critical sections will
 * synchronize on the given object.
 *
 * @param lock  The Object to synchronize on.
 */
	 Reader(std::mutex lock) {
		if (&lock == nullptr) {
			throw  NullPointerException();
		}
		//this->lock = lock;
	}

	 /**
 * Attempts to read characters into the specified character buffer.
 * The buffer is used as a repository of characters as-is: the only
 * changes made are the results of a put operation. No flipping or
 * rewinding of the buffer is performed.
 *
 * @param target the buffer to read characters into
 * @return The number of characters added to the buffer, or
 *         -1 if this source of characters is at its end
 * @throws IOException if an I/O error occurs
 * @throws NullPointerException if target is null
 * @throws ReadOnlyBufferException if target is a read only buffer
 * @since 1.5
 */
		public: int read(java::nio::CharBuffer & target) {
		 int len = target.remaining();
		 ByteArray cbuf (len);
		 int n = read(cbuf, 0, len);
		 if (n > 0)
			 target.put(cbuf, 0, n);
		 return n;
	 }


			  /**
		  * Reads a single character.  This method will block until a character is
		  * available, an I/O error occurs, or the end of the stream is reached.
		  *
		  * <p> Subclasses that intend to support efficient single-character input
		  * should override this method.
		  *
		  * @return     The character read, as an integer in the range 0 to 65535
		  *             (<tt>0x00-0xffff</tt>), or -1 if the end of the stream has
		  *             been reached
		  *
		  * @exception  IOException  If an I/O error occurs
		  */
		public: int read() {
		  ByteArray cb(1);
		  if (read(cb, 0, 1) == -1)
			  return -1;
		  else
			  return cb[0];
		}

		/**
		 * Reads characters into an array.  This method will block until some input
		 * is available, an I/O error occurs, or the end of the stream is reached.
		 *
		 * @param       cbuf  Destination buffer
		 *
		 * @return      The number of characters read, or -1
		 *              if the end of the stream
		 *              has been reached
		 *
		 * @exception   IOException  If an I/O error occurs
		 */
		public: int read(ByteArray& cbuf) {
		  return read(cbuf, 0, cbuf.size());
		}

		/**
		 * Reads characters into a portion of an array.  This method will block
		 * until some input is available, an I/O error occurs, or the end of the
		 * stream is reached.
		 *
		 * @param      cbuf  Destination buffer
		 * @param      off   Offset at which to start storing characters
		 * @param      len   Maximum number of characters to read
		 *
		 * @return     The number of characters read, or -1 if the end of the
		 *             stream has been reached
		 *
		 * @exception  IOException  If an I/O error occurs
		 */
		public: abstract virtual int read(ByteArray& cbuf,const int& off,const int &len)=0;

		/** Maximum skip-buffer size */
		private: static final int maxSkipBufferSize = 8192;

		/** Skip buffer, null until allocated */
		private: ByteArray skipBuffer;

		/**
		 * Skips characters.  This method will block until some characters are
		 * available, an I/O error occurs, or the end of the stream is reached.
		 *
		 * @param  n  The number of characters to skip
		 *
		 * @return    The number of characters actually skipped
		 *
		 * @exception  IllegalArgumentException  If <code>n</code> is negative.
		 * @exception  IOException  If an I/O error occurs
		 */
		public: long skip(long& n)  {
		  if (n < 0L)
			  throw  IllegalArgumentException("skip value is negative");
		  int nn = (int)Math::min_(n, maxSkipBufferSize);
		  synchronized(lock) {
			  if ((skipBuffer.size() < nn))
				  skipBuffer = ByteArray(nn);
			  long r = n;
			  while (r > 0) {
				  int nc = read(skipBuffer, 0, (int)Math::min_(r, nn));
				  if (nc == -1)
					  break;
				  r -= nc;
			  }
			  return n - r;
		  }
		}

		/**
		 * Tells whether this stream is ready to be read.
		 *
		 * @return True if the next read() is guaranteed not to block for input,
		 * false otherwise.  Note that returning false does not guarantee that the
		 * next read will block.
		 *
		 * @exception  IOException  If an I/O error occurs
		 */
		public: bool ready()  {
		  return false;
		}

			/**
		* Tells whether this stream supports the mark() operation. The default
		* implementation always returns false. Subclasses should override this
		* method.
		*
		* @return true if and only if this stream supports the mark operation.
		*/
		public: bool markSupported() {
			return false;
		}

		/**
		 * Marks the present position in the stream.  Subsequent calls to reset()
		 * will attempt to reposition the stream to this point.  Not all
		 * character-input streams support the mark() operation.
		 *
		 * @param  readAheadLimit  Limit on the number of characters that may be
		 *                         read while still preserving the mark.  After
		 *                         reading this many characters, attempting to
		 *                         reset the stream may fail.
		 *
		 * @exception  IOException  If the stream does not support mark(),
		 *                          or if some other I/O error occurs
		 */
		public: void mark(int readAheadLimit)  {
			throw  IOException("mark() not supported");
		}

	    /**
        * Resets the stream.  If the stream has been marked, then attempt to
        * reposition it at the mark.  If the stream has not been marked, then
        * attempt to reset it in some way appropriate to the particular stream,
        * for example by repositioning it to its starting point.  Not all
        * character-input streams support the reset() operation, and some support
        * reset() without supporting mark().
        *
        * @exception  IOException  If the stream has not been marked,
        *                          or if the mark has been invalidated,
        *                          or if the stream does not support reset(),
        *                          or if some other I/O error occurs
        */
		public: void reset()  {
		    throw  IOException("reset() not supported");
	    }

        /**
         * Closes the stream and releases any system resources associated with
         * it.  Once the stream has been closed, further read(), ready(),
         * mark(), reset(), or skip() invocations will throw an IOException.
         * Closing a previously closed stream has no effect.
         *
         * @exception  IOException  If an I/O error occurs
         */
         virtual abstract void close()=0;

		};


		abstract class Writer extends Object {

		private: jchar* writeBuffer = nullptr;
			     static final int WRITE_BUFFER_SIZE;
		protected:		 
			     std::mutex lock;
				 Writer() {}

		public:
				 void write(int c)  {
					 synchronized(lock) {
						 if (writeBuffer == nullptr) {
							 writeBuffer = new jchar[WRITE_BUFFER_SIZE];
						 }
						 writeBuffer[0] = (jchar)c;
						 write(writeBuffer, 0, 1);
					 }
				 }


				 void write(CharArray& cbuf) {
					 write(&cbuf[0], 0, cbuf.size());
				 }

				 abstract  void write(jchar* cbuf,const int& off,const int& len) ;
		};
	}
}