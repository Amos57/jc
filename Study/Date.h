#pragma once
#include "String.h"
#include <ctime>

namespace java {
	namespace util {
		using namespace java::lang;
		class Date
		{
		private:
			long long time;
			std::time_t t;
			std::tm* now = nullptr;


		public:Date(const long long&);
			   Date();
			  long getTime();
			  int getYours();
			  int getHours();
			  int getMinuts();
			  int getSeconds();
			  int getDay();
			  int getDate();
			  int getMonth();
			  String toString();


		};

	}
}