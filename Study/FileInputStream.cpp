#include "FileInputStream.h"

import java::io::IOException;
import java::io::FileInputStream;

FileInputStream::FileInputStream(const String& name) :path(name) ,size(0){
	if (&name == nullptr) {
		throw  NullPointerException();
	}
	open(name);
	size = ou.seekg(0, std::ios::end).tellg();
	ou.seekg(0);
}

FileInputStream::FileInputStream(const FileDescriptor& p) : path(""),size(0) {
	fd = p.handle;
}


void FileInputStream::open(const String& name) {
	ou.open(name.toStdString().c_str(), std::ios::binary);
	
}

int FileInputStream::read() {
	size--;
	if (size < 0)
		return -1;
	ByteArray bt(1);
	int b = readBytes(bt,0,1);
	int test = bt[0];
	return b == -1 ? -1 : test & 0xff;
}

int FileInputStream::readBytes(ByteArray& ba, int off, int len) {
	if (!fd )
		ou.read(&ba[off], len);
	else {
		int res =  ReadFile(fd, &ba[off], len, &(unsigned long&)size, nullptr);
		if (res == 0)
			return -1;
	}
	return ba.size();
}

int FileInputStream::read(ByteArray& ba) {
	return readBytes( ba, 0,size);
}

long FileInputStream::skip(const long& n) {
	ou.ignore(n);
	return size-=n;
}

int FileInputStream::available() {
	return size;
}

void FileInputStream::close() {
	if(fd)
		CloseHandle(fd);
	else
	    ou.close();
}

FileInputStream::~FileInputStream() {
	close();
}
