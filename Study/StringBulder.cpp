#include "StringBulder.h"
#include "Throwable.h"

import java::lang::StringBulder;
import java::lang::IndexOutOfBoundsException;

StringBulder::StringBulder() {
	capast = 16;
	size = 0;
	data = new char[16];
}



StringBulder::StringBulder(const int& len) : size(0) ,capast(len)
{
	data = new char[len];
}

int java::lang::StringBulder::length()
{
	return size;
}

int StringBulder::capacity()
{
	return capast;
}

char StringBulder::charAt(const int& index)
{
	if (index >= length())
		throw IndexOutOfBoundsException("index more then array:"+index);
	return data[index];
}

//CharSequence& CharSequence::subSequence(const int& start, const int& end)
//{
//	return *this;
//}
