#include "Object.h"

#include <sstream>
#include <typeinfo>

#pragma warning(3:4996)

using namespace java::lang;


int Object::hashCode() const{
	return reinterpret_cast<int>(this);
}

Class java::lang::Object::getClass() const
{
	Class cls(this);
	return cls;
}

std::string Object::toString() {
	std::stringstream ss;
	ss << std::hex << hashCode();
	std::string temp = ss.str();
    return	getClass().getName() + "@"+temp;
}



bool Object::equals(const Object& obj) const {
	return this == &obj;
}

bool Object::operator==(const Object& obj) {
	return this->hashCode() == obj.hashCode();
}


// ==============  CLASS ================= //

std::string Class::getName() {
	std::string res = "";
	const char* arr = typeid(*obj).name();
	int len = strlen(arr);
	int count = 0;
	for (int i = 6; i < len; i++) {
		if (':' == arr[i]) {
			count++;
			if (count == 1)
				continue;
			else {
				count = 0;
				res += ".";
				continue;
			}
		}
		res += arr[i];
	}
	return res;
}

std::string Class::getShortName() {
	std::string res = "";
	const char* arr = typeid(*obj).name();
	int len = strlen(arr);
	for (int i= len-1; i >=0; i--) {
		if (':' == arr[i] || ' '== arr[i])
			break;
		res += arr[i];
	}
	std::reverse(res.begin(),res.end());
	return res;
}
