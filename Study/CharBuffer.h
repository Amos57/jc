#pragma once
#include <vector>
#include "String.h"
#include "CharSequence.h"
#include "Buffer.h"
#include "ByteOrder.h"
#include "Math.h"


import java::lang::String;
import java::lang::CharSequence;
import java::nio::ByteOrder;
import java::io::IOException;
import java::lang::Math;



typedef std::vector<jchar> CharArray;


namespace java {
	namespace nio {
	
abstract class  CharBuffer extends  Buffer ,public CharSequence{


protected:
	CharArray hb;                  // Non-null only for heap buffers
	 int offset;
	bool isReadOnlyV;
	int array_offset;

	// Creates a new buffer with the given mark, position, limit, capacity,
  // backing array, and array offset
  //
	CharBuffer(const int& mark,const int& pos,const int& lim,const int& cap,   // package-private
		jchar* h,const int& offset) :
		Buffer(mark, pos, lim, cap),
		
		offset(offset)
	{
		if(h)
			hb.insert(hb.begin(), &h[0], &h[cap - 1]);
	}

	// Creates a new buffer with the given mark, position, limit, and capacity
	//
	CharBuffer(const int& mark, const int& pos, const int& lim, const int& cap) :CharBuffer(mark, pos, lim, cap, nullptr, 0){ // package-private	
	}
public:
	static const CharBuffer& allocate(const int& capacity);

	CharBuffer& append(wchar_t c) {
	
	}
	CharBuffer& put(String& src,const int& start,const int& end) {
		checkBounds(start, end - start, src.length());
		if (isReadOnly())
			throw  1;// ReadOnlyBufferException();
		if (end - start > remaining())
			throw 1;// BufferOverflowException();
		for (int i = start; i < end; i++)
			this->put(src.charAt(i));
		return *this;
	}

	std::string toString() {
		return toString(position(), limit());
	}

	abstract virtual std::string toString(const int& start,const int& end)=0;

	
	jchar charAt(const int& index) {
		return get(position() + checkIndex(index, 1));
	}
	const int length() {
		return remaining();
	}

public:virtual abstract const CharSequence& subSequence(const int& st,const int&)=0;
	int remaining() {
		return limit_ - position_;
	}
	CharBuffer& put(CharArray src,const int& offset,const int& length) {
		checkBounds(offset, length, src.size());
		if (length > remaining())
			throw 1;// new BufferOverflowException();
		int end = offset + length;
		for (int i = offset; i < end; i++)
			  put(src[i]);
		return *this;
	}
	CharBuffer& get(CharArray & dst, int offset, int length) {
		checkBounds(offset, length, dst.size());
		if (length > remaining())
			throw 1;// new BufferUnderflowException();
		int end = offset + length;
		for (int i = offset; i < end; i++)
			dst[i] = get();
		return *this;
	}

	/**
 * Appends a subsequence of the  specified character sequence  to this
 * buffer&nbsp;&nbsp;<i>(optional operation)</i>.
 *
 * <p> An invocation of this method of the form <tt>dst.append(csq, start,
 * end)</tt> when <tt>csq</tt> is not <tt>null</tt>, behaves in exactly the
 * same way as the invocation
 *
 * <pre>
 *     dst.put(csq.subSequence(start, end).toString()) </pre>
 *
 * @param  csq
 *         The character sequence from which a subsequence will be
 *         appended.  If <tt>csq</tt> is <tt>null</tt>, then characters
 *         will be appended as if <tt>csq</tt> contained the four
 *         characters <tt>"null"</tt>.
 *
 * @return  This buffer
 *
 * @throws  BufferOverflowException
 *          If there is insufficient space in this buffer
 *
 * @throws  IndexOutOfBoundsException
 *          If <tt>start</tt> or <tt>end</tt> are negative, <tt>start</tt>
 *          is greater than <tt>end</tt>, or <tt>end</tt> is greater than
 *          <tt>csq.length()</tt>
 *
 * @throws  ReadOnlyBufferException
 *          If this buffer is read-only
 *
 * @since  1.5
*/
public: CharBuffer& append(CharSequence& csq, const int& start, const int& end) {
		// &csq == nullptr ? "null" : &csq;
		//return put(csq.subSequence(start, end).toString());
	return *this;
	}
 

 /**
* Attempts to read characters into the specified character buffer.
* The buffer is used as a repository of characters as-is: the only
* changes made are the results of a put operation. No flipping or
* rewinding of the buffer is performed.
*
* @param target the buffer to read characters into
* @return The number of characters added to the buffer, or
*         -1 if this source of characters is at its end
* @throws IOException if an I/O error occurs
* @throws NullPointerException if target is null
* @throws ReadOnlyBufferException if target is a read only buffer
* @since 1.5
*/
 int read(CharBuffer& target) {
		// Determine the number of bytes n that can be transferred
		int targetRemaining = target.remaining();
		int rem = remaining();
		if (rem == 0)
			return -1;
		int n = Math::min_(rem, targetRemaining);
		int lim = limit();
		// Set source limit to prevent target overflow
		if (targetRemaining < rem)
			limit(position() + n);
		try {
			if (n > 0)
				target.put(*this);
		}catch (IOException& e) {
			e.what();
		}
	
			limit(lim); // restore real limit
		return n;
	}

/**
* Wraps a character sequence into a buffer.
*
* <p> The content of the new, read-only buffer will be the content of the
* given character sequence.  The buffer's capacity will be
* <tt>csq.length()</tt>, its position will be <tt>start</tt>, its limit
* will be <tt>end</tt>, and its mark will be undefined.  </p>
*
* @param  csq
*         The character sequence from which the new character buffer is to
*         be created
*
* @param  start
*         The index of the first character to be used;
*         must be non-negative and no larger than <tt>csq.length()</tt>.
*         The new buffer's position will be set to this value.
*
* @param  end
*         The index of the character following the last character to be
*         used; must be no smaller than <tt>start</tt> and no larger
*         than <tt>csq.length()</tt>.
*         The new buffer's limit will be set to this value.
*
* @return  The new character buffer
*
* @throws  IndexOutOfBoundsException
*          If the preconditions on the <tt>start</tt> and <tt>end</tt>
*          parameters do not hold
*/
 static CharBuffer& wrap(CharSequence& csq,const int& start,const int& end) {
		try {
			//return  StringCharBuffer(csq, start, end);
		}catch (IllegalArgumentException x) {
			throw  IndexOutOfBoundsException();
		}
	}
  bool hasArray() {
	 return (hb.size() != 0) && !isReadOnly();
 }

   int arrayOffset() {
	   if (hb.size() != 0)
		   throw 1;// UnsupportedOperationException();
	   if (isReadOnly())
		   throw 2;// ReadOnlyBufferException();
	  return offset;
  }

    jchar* array() {
	   if (hb.size() != 0)
		   throw 1;// new UnsupportedOperationException();
	   if (isReadOnly())
		   throw 2;// new ReadOnlyBufferException();
	   return &hb[0];
   }
 /**
	 * Relative bulk <i>put</i> method&nbsp;&nbsp;<i>(optional operation)</i>.
	 *
	 * <p> This method transfers the chars remaining in the given source
	 * buffer into this buffer.  If there are more chars remaining in the
	 * source buffer than in this buffer, that is, if
	 * <tt>src.remaining()</tt>&nbsp;<tt>&gt;</tt>&nbsp;<tt>remaining()</tt>,
	 * then no chars are transferred and a {@link
	 * BufferOverflowException} is thrown.
	 *
	 * <p> Otherwise, this method copies
	 * <i>n</i>&nbsp;=&nbsp;<tt>src.remaining()</tt> chars from the given
	 * buffer into this buffer, starting at each buffer's current position.
	 * The positions of both buffers are then incremented by <i>n</i>.
	 *
	 * <p> In other words, an invocation of this method of the form
	 * <tt>dst.put(src)</tt> has exactly the same effect as the loop
	 *
	 * <pre>
	 *     while (src.hasRemaining())
	 *         dst.put(src.get()); </pre>
	 *
	 * except that it first checks that there is sufficient space in this
	 * buffer and it is potentially much more efficient.
	 *
	 * @param  src
	 *         The source buffer from which chars are to be read;
	 *         must not be this buffer
	 *
	 * @return  This buffer
	 *
	 * @throws  BufferOverflowException
	 *          If there is insufficient space in this buffer
	 *          for the remaining chars in the source buffer
	 *
	 * @throws  IllegalArgumentException
	 *          If the source buffer is this buffer
	 *
	 * @throws  ReadOnlyBufferException
	 *          If this buffer is read-only
	 */
  CharBuffer& put(CharBuffer& src) {
	 if (&src == this)
		 throw  IllegalArgumentException();
	 if (isReadOnly())
		 throw 1;//new ReadOnlyBufferException();
	 int n = src.remaining();
	 if (n > remaining())
		 throw 1;// new BufferOverflowException();
	 for (int i = 0; i < n; i++)
		 put(src.get());
	 return *this;
 }

/**
* Wraps a character sequence into a buffer.
*
* <p> The content of the new, read-only buffer will be the content of the
* given character sequence.  The new buffer's capacity and limit will be
* <tt>csq.length()</tt>, its position will be zero, and its mark will be
* undefined.  </p>
*
* @param  csq
*         The character sequence from which the new character buffer is to
*         be created
*
* @return  The new character buffer
*/
static CharBuffer& wrap( CharSequence& csq) {
	return wrap(csq, 0, csq.length());
}

 /**
* Creates a new char buffer whose content is a shared subsequence of
* this buffer's content.
*
* <p> The content of the new buffer will start at this buffer's current
* position.  Changes to this buffer's content will be visible in the new
* buffer, and vice versa; the two buffers' position, limit, and mark
* values will be independent.
*
* <p> The new buffer's position will be zero, its capacity and its limit
* will be the number of chars remaining in this buffer, and its mark
* will be undefined.  The new buffer will be direct if, and only if, this
* buffer is direct, and it will be read-only if, and only if, this buffer
* is read-only.  </p>
*
* @return  The new char buffer
*/
 abstract virtual const CharBuffer& slice()=0;

/**
 * Creates a new char buffer that shares this buffer's content.
 *
 * <p> The content of the new buffer will be that of this buffer.  Changes
 * to this buffer's content will be visible in the new buffer, and vice
 * versa; the two buffers' position, limit, and mark values will be
 * independent.
 *
 * <p> The new buffer's capacity, limit, position, and mark values will be
 * identical to those of this buffer.  The new buffer will be direct if,
 * and only if, this buffer is direct, and it will be read-only if, and
 * only if, this buffer is read-only.  </p>
 *
 * @return  The new char buffer
 */
 abstract virtual CharBuffer& duplicate()=0;

 /**
* Creates a new, read-only char buffer that shares this buffer's
* content.
*
* <p> The content of the new buffer will be that of this buffer.  Changes
* to this buffer's content will be visible in the new buffer; the new
* buffer itself, however, will be read-only and will not allow the shared
* content to be modified.  The two buffers' position, limit, and mark
* values will be independent.
*
* <p> The new buffer's capacity, limit, position, and mark values will be
* identical to those of this buffer.
*
* <p> If this buffer is itself read-only then this method behaves in
* exactly the same way as the {@link #duplicate duplicate} method.  </p>
*
* @return  The new, read-only char buffer
*/
abstract virtual CharBuffer& asReadOnlyBuffer()=0;

/**
* Relative <i>get</i> method.  Reads the char at this buffer's
* current position, and then increments the position.
*
* @return  The char at the buffer's current position
*
* @throws  BufferUnderflowException
*          If the buffer's current position is not smaller than its limit
*/
abstract virtual jchar get(const int& index)=0;

abstract virtual jchar get() = 0;

 /**
  * Relative <i>put</i> method&nbsp;&nbsp;<i>(optional operation)</i>.
  *
  * <p> Writes the given char into this buffer at the current
  * position, and then increments the position. </p>
  *
  * @param  c
  *         The char to be written
  *
  * @return  This buffer
  *
  * @throws  BufferOverflowException
  *          If this buffer's current position is not smaller than its limit
  *
  * @throws  ReadOnlyBufferException
  *          If this buffer is read-only
  */
  abstract virtual CharBuffer& put(const jchar& c)=0;

	/**
  * Appends the specified char  to this
  * buffer&nbsp;&nbsp;<i>(optional operation)</i>.
  *
  * <p> An invocation of this method of the form <tt>dst.append(c)</tt>
  * behaves in exactly the same way as the invocation
  *
  * <pre>
  *     dst.put(c) </pre>
  *
  * @param  c
  *         The 16-bit char to append
  *
  * @return  This buffer
  *
  * @throws  BufferOverflowException
  *          If there is insufficient space in this buffer
  *
  * @throws  ReadOnlyBufferException
  *          If this buffer is read-only
  *
  * @since  1.5
  */
    CharBuffer& append(const jchar& c) {
		return put(c);
	}

	   /**
	* Retrieves this buffer's byte order.
	*
	* <p> The byte order of a char buffer created by allocation or by
	* wrapping an existing <tt>char</tt> array is the {@link
	* ByteOrder#nativeOrder native order} of the underlying
	* hardware.  The byte order of a char buffer created as a <a
	* href="ByteBuffer.html#views">view</a> of a byte buffer is that of the
	* byte buffer at the moment that the view is created.  </p>
	*
	* @return  This buffer's byte order
	*/
	abstract virtual ByteOrder order()=0;

	~CharBuffer() {

	}
};
}
}