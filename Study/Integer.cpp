#include "Integer.h"


using namespace java::lang;

const int Integer::MAX_VALUE = 0x7fffffff;
const int Integer::MIN_VALUE = 0x80000000;

const  char Integer::DigitOnes[] = {
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
};

const char Integer::digits[36] = {
		'0' , '1' , '2' , '3' , '4' , '5' ,
		'6' , '7' , '8' , '9' , 'a' , 'b' ,
		'c' , 'd' , 'e' , 'f' , 'g' , 'h' ,
		'i' , 'j' , 'k' , 'l' , 'm' , 'n' ,
		'o' , 'p' , 'q' , 'r' , 's' , 't' ,
		'u' , 'v' , 'w' , 'x' , 'y' , 'z'
};

const  char Integer::DigitTens[] = {
		'0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
		'1', '1', '1', '1', '1', '1', '1', '1', '1', '1',
		'2', '2', '2', '2', '2', '2', '2', '2', '2', '2',
		'3', '3', '3', '3', '3', '3', '3', '3', '3', '3',
		'4', '4', '4', '4', '4', '4', '4', '4', '4', '4',
		'5', '5', '5', '5', '5', '5', '5', '5', '5', '5',
		'6', '6', '6', '6', '6', '6', '6', '6', '6', '6',
		'7', '7', '7', '7', '7', '7', '7', '7', '7', '7',
		'8', '8', '8', '8', '8', '8', '8', '8', '8', '8',
		'9', '9', '9', '9', '9', '9', '9', '9', '9', '9',
};

Integer::Integer() : value(0) {}


Integer::Integer(const int& val) : value(val) { }


std::ostream& java::lang::operator<< (std::ostream& os, const Integer& that) {
	os << that.value;
	return os;
}
String Integer::toString() {
	return String::valueOf(value);
}
static int stringSize(int x) {
	unsigned int number_of_digits = 0;

	do {
		++number_of_digits;
		x /= 10;
	} while (x);
			return number_of_digits;
}
 void java::lang::Integer::getChars(int& i, int& index, char* buf) {
	int q, r;
	int charPos = index;
	char sign = 0;

	if (i < 0) {
		sign = '-';
		i = -i;
	}
		while (i >= 65536) {
			q = i / 100;
			// really: r = i - (q * 100);
			r = i - ((q << 6) + (q << 5) + (q << 2));
			i = q;
			buf[--charPos] = DigitOnes[r];
			buf[--charPos] = DigitTens[r];
		}

		// Fall thru to fast mode for smaller numbers
		// assert(i <= 65536, i);
		for (;;) {
			q = (i * 52429) >> (16 + 3);
			r = i - ((q << 3) + (q << 1));  // r = i-(q*10) ...
			buf[--charPos] = digits[r];
			i = q;
			if (i == 0) break;
		}
		if (sign != 0) {
			buf[--charPos] = sign;
		}
	}

String java::lang::Integer::toString(const int& i)
{
	if (i == Integer::MIN_VALUE)
		return "-2147483648";
	int size = (i < 0) ? stringSize(-i) + 1 : stringSize(i);
	char*buf = new char[size+1];
	getChars(*const_cast<int*>(&i), size, buf);
	buf[size] = '\0';
	String ar(buf);
	delete[] buf;
	return ar;
}

int Integer::parseInt(const String& num) {
	std::stringstream s;
	s << num;
	int res;
	s >> res;
	return res;
}


Integer& Integer::operator=(const int& init) {
	value = init;
	return *this;
}
Integer& Integer::operator=(const Integer& that) {
	value = that.value;
	return *this;
}
Integer& Integer::operator+=(const Integer& that) {
		value += that.value;
		return *this;
	}
Integer& Integer::operator/=(const Integer& that) {
	value /= that.value;
	return *this;
}
Integer& Integer::operator*=(const Integer& that) {	
	value *= that.value;
	return *this;
}
Integer& Integer::operator%=(const Integer& that) {
	value %= that.value;
	return *this;
}
Integer& Integer::operator-=(const Integer& that) {
	value -= that.value;
	return *this;
}
Integer& Integer::operator+(const Integer& that) {
	Integer i(that);
	i=value +that.value;
	return i;
}
Integer::operator int() const{
	return value;
}

Integer& Integer::operator--() {
	value--;
	return *this;
}
Integer& Integer::operator++() {
	value++;
	return *this;
}
bool Integer::operator!=(const Integer& that) {
	return  that.value!=value ;
}

String Integer::toString( int val,  int radix) {
	if (radix < 2 || radix > 36)
		radix = 10;

	/* Use the faster version */
	if (radix == 10) {
		return toString(val);
	}

	char buf[33+1];
	bool negative = (val < 0);
	int charPos = 32;

	if (!negative) {
		val = -val;
	}

	while (val <= -radix) {
		buf[charPos--] = digits[-(val % radix)];
		val = val / radix;
	}
	buf[charPos] = digits[-val];

	if (negative) {
		buf[--charPos] = '-';
	}
	buf[33] = '\0';
	return  String(buf, charPos, (33 - charPos));

}

int Integer::intValue() {
	return value;
}
String Integer::toHexString(const int& val) {
	return toString(val,16);
}