#include "PrintStream.h"



import java::io::PrintStream;

PrintStream::PrintStream(const bool & autoFlush, OutputStream* out)
	: FilterOutputStream(out),
	autoFlush(autoFlush),
	trouble(false),
	closing(false)
{


	//this.charOut = new OutputStreamWriter(this);
	//this.textOut = new BufferedWriter(charOut);
}

PrintStream::PrintStream( OutputStream* out) : PrintStream(false,out)
	//: FilterOutputStream(out),
	//autoFlush(false) 
{}


bool PrintStream::checkError() {
	if (out != nullptr)
		flush();
	if (PrintStream* ps=dynamic_cast<PrintStream*>(out)) {
		return ps->checkError();
	}
	return trouble;
}
void PrintStream::flush() {
	std::lock_guard<std::mutex> lg(m);
		try {
			ensureOpen();
			out->flush();
		}
		catch (IOException &x) {
			trouble = true;
		}
	
}
void  PrintStream::close() {
	synchronized(m){

		if (!closing) {
			closing = true;
			try {
				//textOut.close();
				//out.close();
			}catch (IOException& x) {
				trouble = true;
			}
			//textOut = null;
			//charOut = null;
			out = nullptr;
		}
		
	}

}
PrintStream::~PrintStream() {
	close();
}
