#pragma once

#include <mutex>
#include <string>
#ifdef interface
#undef interface
#endif // interface

#ifdef implements
#undef implements
#endif // implements

#ifdef extends
#undef extends
#endif // extends

#ifdef final
#undef final
#endif // final

#ifdef abstract
#undef abstract
#endif // abstract

#ifdef import
#undef import
#endif // import

#ifdef OBJECT
#undef OBJECT
#endif // OBJECT

//#define _@Override

#define implements :public
#define extends    :public
#define interface  class
#define abstract
#define final      const
#define import     using
#define synchronized(m) std::lock_guard<std::mutex> lg(m);

typedef wchar_t jchar;




#define OBJECT


namespace java {
	namespace lang {
		class Class;
		class Object 
		{
		public:

			virtual int hashCode() const;
			virtual Class getClass() const;
			virtual bool equals(const Object& obj)  const;
			virtual std::string toString() ;

			bool operator==(const Object& obj);
		};

		class Class extends Object {
		private:
			const char* name=0;
			const char* shortName=0;
			const Object* obj=0;
			

		public:
            Class(const Object* o):obj(o){}
			std::string getName();
			std::string getShortName();
			
		};

	}
}

