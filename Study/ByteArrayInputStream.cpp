#include "ByteArrayInputStream.h"
#include "Throwable.h"

using namespace java::io;
using namespace java::lang;


ByteArrayInputStream::ByteArrayInputStream(ByteArray& buf) 
	: buf(&buf) ,pos(0),count(buf.size()),mark_(0)
     {}

ByteArrayInputStream::ByteArrayInputStream(ByteArray& buf, const int& offset, const int& length) {
	this->buf = &buf;
	this->pos = offset;
	this->count = java::lang::Math::min_(offset + length, buf.size());
	this->mark_ = offset;
}

int ByteArrayInputStream::read() {
	//ByteArray & ba = *buf;
	return (pos < count) ? (buf->operator[](pos++) & 0xff) : -1;
}


int ByteArrayInputStream::read(ByteArray& b,  int off,  int len) {

 if (off < 0 || len < 0 || len > buf->size() - off) {
		throw  IndexOutOfBoundsException();
	}

	if (pos >= count) {
		return -1;
	}

	int avail = count - pos;
	if (len > avail) {
		len = avail;
	}
	if (len <= 0) {
		return 0;
	}
	memcpy(&buf->operator[](pos),&b[off], len);
	//System::arraycopy(buf, pos, b, off, len);
	pos += len;
	return len;
}


long ByteArrayInputStream::skip(const long& n) {
	long k = count - pos;
	if (n < k) {
		k = n < 0 ? 0 : n;
	}

	pos += k;
	return k;
}




/**
 * Tests if this <code>InputStream</code> supports mark/reset. The
 * <code>markSupported</code> method of <code>ByteArrayInputStream</code>
 * always returns <code>true</code>.
 *
 * @since   JDK1.1
 */
bool ByteArrayInputStream::markSupported() {
	return true;
}

/**
 * Set the current marked position in the stream.
 * ByteArrayInputStream objects are marked at position zero by
 * default when constructed.  They may be marked at another
 * position within the buffer by this method.
 * <p>
 * If no mark has been set, then the value of the mark is the
 * offset passed to the constructor (or 0 if the offset was not
 * supplied).
 *
 * <p> Note: The <code>readAheadLimit</code> for this class
 *  has no meaning.
 *
 * @since   JDK1.1
 */
void ByteArrayInputStream::mark(const int& readAheadLimit) {
	mark_ = pos;
}

/**
 * Resets the buffer to the marked position.  The marked position
 * is 0 unless another position was marked or an offset was specified
 * in the constructor.
 */
 void ByteArrayInputStream::reset() {
	pos = mark_;
}

/**
 * Closing a <tt>ByteArrayInputStream</tt> has no effect. The methods in
 * this class can be called after the stream has been closed without
 * generating an <tt>IOException</tt>.
 */
void ByteArrayInputStream::close() {
}