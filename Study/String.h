#pragma once

#include <ostream>
#include <sstream>
#include <vector>
#include "Object.h"



namespace java {
	namespace lang {
class String extends Object
{

private:
	char* value;
	int size;
public:
	String(const char * init);
	String(const jchar* init);
	String(const String& that);
	String(char* init,const int&,const int&);
	String(jchar* init, const int&, const int&);

	String& operator=(const char* init);
	String& operator=(const wchar_t* init);
	String& operator=(const String& that);
	String& operator+=(const String& that);
	String& operator+(const int& that);
	bool operator==(std::nullptr_t null);
	operator const char* ();
	String& operator +(const String& that);
	String& operator +(const char* that);

	friend  std::ostream& operator<< (std::ostream& os,const String& that);

	bool isEmpty();

	int indexOf( char ch) const;

	int indexOf(const String& val);

	char charAt(const int& index) const;

	bool startsWith(const String& prefix);
	bool startsWith(const String& prefix,const int& set);

	bool endWith(const String& prefix);

	String trim();
	String toLowerCase();
	String toUpperCase();
	String substring(const int&i,const int& end) const;
	
	String substring(const int& end) const;

    String replace(const String& lastElem, const String& newElem);
	String replaceFirst(const String& lastElem, const String& newElem);
	String replaceLast(const String& lastElem, const String& newElem);
	String replaceAll(const String& lastElem, const String& newElem);

	

	static String valueOf(const long& l);

	static String valueOf(const int& l);

	static String valueOf(const bool& l);

	static String valueOf(const float& l);

	static String valueOf(const double& l);

	int lastIndexOf(const String& element);

	bool equals(const String& element);

	bool contains(const String& el);

	std::string toStdString() const;

	int length() const;

	std::wstring toWString() const;

	std::vector<String> split(const char& val) const;
	std::vector<String> split(const char* val) const;

	std::vector<char> toCharArray() const;

	~String();
};
	}
}
