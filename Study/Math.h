#pragma once
#ifndef OBJECT
#include "Object.h"
#endif // !OBJECT

namespace java {
	namespace lang {
		class Math : public Object
		{
		public:
			static int min_(const int &ar1, const int& ar2) {
				return ar1 >= ar2 ? ar1 : ar2;
			}


		};
	}
}