#include "String.h"

#include <iostream>
#include <vector>

#pragma warning(3:4996)

template<class T>
std::string numberToString(const T& t) {
	std::stringstream ss;
	ss << t;
	return ss.str();
}

using namespace java::lang;

//String::String(const jchar* init)
//{
//	size = wcslen(init)+1;
//	value = new char[size];
//	size_t len = wcstombs(value, init, size);
//	value[size] = '\0';
//}
String::String(const char* init) {
	size = strlen(init);
	value = new char[size + 1];
	memcpy(value, init, size);
	value[size] = '\0';

}
String::String(const String& that) {

	size = that.size;
	value = new char[size + 1];
	memcpy(value, that.value, size);
	value[size] = '\0';
}



String::String(char* init, const int&st, const int&len) {
	size =   len;
	value = new char[size+1];
	memcpy(value,&init[st],size);
	value[size] = '\0';

}

String::String(jchar* init, const int& st, const int& len) {
	size = len;
	value = new char[size + 1];
	size_t l = wcstombs(value, init, size);
	value[size] = '\0';

}

String::String(const jchar* init) {
	size = wcslen(init);
	value = new char[size + 1];
	size_t len = wcstombs(value, init, size);
	value[size] = '\0';
}


String& String::operator=(const char* init) {
	delete[] value;
	size = strlen(init);
	value = new char[size + 1];
	memcpy(value, init, size);
	value[size] = '\0';
	return *this;
}
String& String::operator=(const String& that) {
	delete[] value;
	size = that.size;
	value = new char[size + 1];
	memcpy(value, that.value, size);
	value[size] = '\0';
	return *this;
}
String& String::operator+=(const String& that) {
	int newSize = size + that.size;
	char* newValue = new char[newSize + 1];

	memcpy(newValue, value, size);
	memcpy(&newValue[size], that.value, that.size);
	delete[] value;

	value = new char[newSize + 1];
	memcpy(value, newValue, newSize);


	delete[] newValue;

	size = newSize;
	value[size] = '\0';
	return *this;
}
String& String::operator+(const int& that) {
	std::stringstream ss;
	ss << that;
	std::string res = ss.str();
	int newSize = res.length() + size;
	const char* numstr = res.c_str();
	char* newValue = new char[newSize + 1];
	memcpy(newValue, value, size);
	memcpy(&newValue[size], numstr, res.length());
	delete[] value;

	value = new char[newSize + 1];
	memcpy(value, newValue, newSize);
	delete[] newValue;

	size = newSize;
	value[size] = '\0';
	return *this;
}

String::operator const char* () {
//char* temp = new char[size+1];
//memcpy(temp,value,size);
//temp[size] = '\0';
	return value;
}

std::ostream& java::lang::operator<< (std::ostream& os, const String& that) {
	os << that.value;
	return os;
}

bool String::operator==(std::nullptr_t null) {
	return value == null;
}



bool String::isEmpty() {
	return size == 0;
}


int String::indexOf(char ch) const {

	for (int i = 0; i < size; i++) {
		if (value[i] == ch)
			return i;
	}
	return -1;
}


int String::indexOf(const String& val) {
	int j;
	int i = 0;
	for (; i < size; i++) {
    j = 0;
		if (value[i] == val.charAt(j)) {
			int clonI = i;
			//clonI ++;
			//j ++;
			for (; j < val.size; clonI++, j++) {
				if (value[clonI] != val.charAt(j)) {
					break;
				}else {
					if (j + 1 == val.size)
						return i;
				}
			}
			

		}
            
	}

	return -1;
}

char String::charAt(const int& index) const {
	return value[index];
}


bool String::startsWith(const String& prefix, const int& set) {
	for (int i = set; i < prefix.size; i++) {
		if (value[i] != prefix.value[i])
			return false;
	}
	return true;
}

bool String::startsWith(const String& prefix) {
	return startsWith(prefix,0);
}

bool String::endWith(const String& prefix) {
	for (int i = size - 1, j = prefix.size - 1; j >= 0; j--, i--) {
		if (value[i] != prefix.value[j])
			return false;

	}
	return true;
}




String String::substring(const int& st, const int& end)const {
	int limit = end - st;
	char* temp = new char[limit + 1];
	memcpy(temp, &value[st], limit);
	temp[limit] = '\0';
	String newStr(temp);
	delete[] temp;
	return newStr;
}


String String::substring(const int& st)const {
	return substring(st, size);
}

String String::replace(const String& lastElem, const String& newElem) {
	return replaceFirst(lastElem, newElem);
}

String String::replaceFirst(const String& lastElem, const String& newElem) {
	int newLen = 0;

	int	i = indexOf(lastElem);
		if (i == -1)
			return *this;

		newLen += i;
		for (int j = 0; j < newElem.length(); j++) {
			value[i] = newElem.charAt(j);
			i++;

		}
		newLen = size - lastElem.length() + newElem.length();
		char* temp = new char[newLen + 1];
		memcpy(temp, value, newLen);
		temp[newLen] = '\0';
		delete[] value;
		value = *&temp;

	return *this;
}


String String::replaceLast(const String& lastElem, const String& newElem) {
	return "";
}
String String::replaceAll(const String& lastElem, const String& newElem) {
	int newLen=0;
	unsigned count=0;
	for (int i = 0; i < size;) {
		i = indexOf(lastElem);
		if (i == -1) 
			break;
		
		newLen += i;
		for (int j = 0; j < newElem.length(); j++) {
			value[i] = newElem.charAt(j);
			i++;
			count++;
		}
	}
	if (count != 0) {
		newLen = size-(count * lastElem.length())+(count*newElem.length());
		char* temp = new char[newLen+1];
		memcpy(temp,value, newLen);
		temp[newLen] = '\0';
		delete[] value;
		value = *&temp;
	}

	return *this;
}

String String::valueOf(const long& l) {
	return numberToString(l).c_str();
}

String String::valueOf(const int& l) {
	return numberToString(l).c_str();
}

String String::valueOf(const bool& l) {
	return l ? "true" : "false";
}
String String::valueOf(const float& l) {
	return numberToString(l).c_str();
}

String String::valueOf(const double& l) {
	return numberToString(l).c_str();
}


String& String::operator=(const wchar_t* init) {
	String temp(init);
	delete[] this->value;
	value = new char[temp.size+1];
	this->size = temp.size;

	memcpy(this->value, temp.value, this->size);
	this->value[size] = '\0';
	return *this;
}

int String::lastIndexOf(const String& element) {
	int j = 0;

	for (int i = size - 1; i >= 0; i--) {
		j = element.size-1;

		if (value[i] == element.charAt(j)) {
			int clonI = i;

			for(; j >= 0; clonI--, j--) 
				if (value[clonI] != element.charAt(j)) {
					break;
				}
				else {
					if (j  == 0)
						return i- element.length()+1;
				}
			}
		}
	
	return -1;
}


bool String::equals(const String& element) {
	if (size != element.size)
		return false;

	for (int i = 0; i < size; i++) {
		if (value[i] != element.charAt(i))
			return false;
	}
	return true;
}
bool String::contains(const String& el) {
	return indexOf(el) != -1;
}

int String::length() const{
	return size;
}

std::wstring String::toWString() const {
	std::wstring wc(size, L'#');
	size_t outSize;
	mbstowcs_s(&outSize,&wc[0],size+1, value,size );
	return  wc;
}

std::string String::toStdString() const{
	std::string s(value);;
	return s;
}


std::vector<char> String::toCharArray() const{
	std::vector<char> v(size);
	v.insert(v.begin(),value,&value[size]);
	return v;
}

std::vector<String> String::split(const char& val) const {
	std::vector<String> array;
	String temp(value);
	int index;
	while ((index = temp.indexOf(val)) != -1) {

	String element = temp.substring(0, index);
	temp = temp.substring(index+1, size);
	array.push_back(element);

	}
	array.push_back(temp);
	return array;
}

std::vector<String> String::split(const char* val) const {
	std::vector<String> array;
	String temp(value);
	int index;
	while ((index = temp.indexOf(val)) != -1) {

		String element = temp.substring(0, index);
		temp = temp.substring(index + 1, size);
		array.push_back(element);

	}
	array.push_back(temp);
	return array;
}


String& String::operator+(const char* that)
{
	int sThat;
	int newLen = size + (sThat=strlen(that));
	char* temp = new char[newLen + 1];
	memcpy(temp, this->value, size);
	delete[] this->value;
	value = new char[newLen + 1];
	memcpy(&temp[size], that, sThat);
	memcpy(value, temp, newLen);
	value[newLen] = '\0';
	size = newLen;
	delete[] temp;
	return *this;
}

String& String::operator+(const String& that)
{
	int newLen = size + that.length();
	char* temp = new char[newLen+1];
	memcpy(temp,this->value,size);
	delete[] this->value;
	value = new char[newLen+1];
	memcpy(&temp[size], that.value, that.length());
	memcpy(value,temp,newLen);
	value[newLen] = '\0';
	size = newLen;
	delete[] temp;
	return *this;
}


String String::trim() {
	int i = 0;
	int j = size - 1;
	while (i != j) {
	
		if (' ' == value[i])
			i++;
		if (' ' == value[j])
			j--;
		if (' ' != value[i] && ' ' != value[j])
			return substring(i,j+1);
	}
}

String String::toLowerCase() {
	for (int i = 0; i < size; i++) {
		if ((value[i] >= 65 && value[i] <= 90) || (value[i] >= -64 && value[i] <= -33))
			value[i] += 32;
	}

	return String(value);
}
String String::toUpperCase() {
	for (int i = 0; i < size; i++) {
		if ((value[i] >= 97 && value[i] <= 122) ||(value[i]>=-32 && value[i] <= -1))
			
			value[i] -= 32;
	}

	return String(value);
}

String::~String() {
	delete[] value;
}