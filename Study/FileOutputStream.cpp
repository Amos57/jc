#include "FileOutputStream.h"
#include <Windows.h>

import java::io::FileOutputStream;
import java::io::FileDescriptor;

FileOutputStream::FileOutputStream(const String& path)
{
	deskr = CreateFile(path.toWString().c_str(), GENERIC_WRITE, 0, NULL,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (INVALID_HANDLE_VALUE == deskr)
		throw IOException("File not opened");

}

FileOutputStream::FileOutputStream(const String& path,const bool &append) {

	if(append)
	    deskr = CreateFile(path.toWString().c_str(), FILE_APPEND_DATA,0
		                  /*FILE_SHARE_WRITE*/, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	else
		deskr = CreateFile(path.toWString().c_str(), GENERIC_WRITE, 0, NULL,
			CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (INVALID_HANDLE_VALUE==deskr)
		throw IOException("File not opened");
}

FileOutputStream::FileOutputStream(const FileDescriptor& fd) {
	deskr = fd.handle;
}

void FileOutputStream::write(const int& b) {
	ByteArray data{ (char)b };
	write(data,0,1);
}

void FileOutputStream::write(const ByteArray& buf, const int& offset, const int& len) {
	if(!WriteFile(deskr,&buf[offset],len,NULL,NULL))
		throw IOException("error writing file");
}
void FileOutputStream::write(const ByteArray& buf) {
	write(buf, 0, buf.size());
}

void FileOutputStream::close() {
	if (deskr)
		CloseHandle(deskr);
}

FileOutputStream::~FileOutputStream() {
	close();
}