#pragma once

#include "String.h"
#include "FilterOutputStream.h"
#include <mutex>


import java::lang::String;
import java::lang::Object;
import java::lang::NullPointerException;

namespace java {
	namespace io {
class PrintStream : public FilterOutputStream
{

private:
	const bool  autoFlush;
	      bool  trouble;
		  bool  closing;
		  std::mutex m;

		  
    void ensureOpen()  {
		if (out == nullptr)
			throw new java::io::IOException("Stream closed");
	}
	template<class T>
	static  T requireNonNull(const T& obj,const String& message) {
		if (&obj == nullptr)
			throw  NullPointerException(message);
		return obj;
	}
public:
	PrintStream(const bool& autoFlush, OutputStream* os);
	PrintStream(OutputStream* out);
	void flush();
	void close();

	void write(const int& b) {
		out->write(b);
	}

	void write(ByteArray& buf, int off, int len);
	void write(char* buf);
	void write(const char& buf);
	void write(const String &s);




	void print(const bool& b) {
		write(b ? "true" : "false");
	}

	void print(const char& c) {
		write(c);
	}
	void print(const int& i) {
		write(String::valueOf(i));
	}

	void print(const long& l) {
		write(String::valueOf(l));
	}
	void setError() {
		trouble = true;
	}

	

	void clearError() {
		trouble = false;
	}
	bool checkError();

	~PrintStream();
};
}
}