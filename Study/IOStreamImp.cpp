#include "IOStreamAbst.h"



using namespace java::io;
using namespace java::lang;


//=============INPUT STREAM========================//


const int InputStream::MAX_SKIP_BUFFER_SIZE = 2048;


int InputStream::read(ByteArray& b) {
	return read(b, 0, b.size());
}

int InputStream::read(ByteArray& b,const int& off, const int& len) {

	if (off < 0 || len < 0 || len > b.size() - off) {
		throw  IndexOutOfBoundsException();
	}
	else if (len == 0) {
		return 0;
	}

	int c = read();
	if (c == -1) {
		return -1;
	}
	b[off] = (char)c;

	int i = 1;
	try {
		for (; i < len; i++) {
			c = read();
			if (c == -1) {
				break;
			}
			b[off + i] = (char)c;
		}
	}
	catch (IOException& ee) {
	}
	return i;
}

long InputStream::skip( long& n)  {

	long remaining = n;
	int nr;

	if (n <= 0) {
		return 0;
	}

	int size = Math::min_(MAX_SKIP_BUFFER_SIZE, remaining);
	ByteArray skipBuffer(size);
	while (remaining > 0) {
		nr = read(skipBuffer, 0, Math::min_(size, remaining));
		if (nr < 0) {
			break;
		}
		remaining -= nr;
	}

	return n - remaining;
}

 int InputStream::available() {
	return 0;

}

 void InputStream::close() {}

 void InputStream::mark(const int& readlimit) {}









 //================OUTPUT STREAM========================//

  void OutputStream::write(ByteArray & b)  {
	 write(b, 0, b.size());
 }

 /**
  * Writes <code>len</code> bytes from the specified byte array
  * starting at offset <code>off</code> to this output stream.
  * The general contract for <code>write(b, off, len)</code> is that
  * some of the bytes in the array <code>b</code> are written to the
  * output stream in order; element <code>b[off]</code> is the first
  * byte written and <code>b[off+len-1]</code> is the last byte written
  * by this operation.
  * <p>
  * The <code>write</code> method of <code>OutputStream</code> calls
  * the write method of one argument on each of the bytes to be
  * written out. Subclasses are encouraged to override this method and
  * provide a more efficient implementation.
  * <p>
  * If <code>b</code> is <code>null</code>, a
  * <code>NullPointerException</code> is thrown.
  * <p>
  * If <code>off</code> is negative, or <code>len</code> is negative, or
  * <code>off+len</code> is greater than the length of the array
  * <code>b</code>, then an <tt>IndexOutOfBoundsException</tt> is thrown.
  *
  * @param      b     the data.
  * @param      off   the start offset in the data.
  * @param      len   the number of bytes to write.
  * @exception  IOException  if an I/O error occurs. In particular,
  *             an <code>IOException</code> is thrown if the output
  *             stream is closed.
  */
  void OutputStream::write(ByteArray & b,const int& off, const int& len) {

	  if ((off < 0) || (off > b.size()) || (len < 0) ||
		 ((off + len) > b.size()) || ((off + len) < 0)) {
		 throw  IndexOutOfBoundsException();
	 }
	 else if (len == 0) {
		 return;
	 }
	 for (int i = 0; i < len; i++) {
		 write((b[off + i] & 0xff));
	 }
 }

 /**
  * Flushes this output stream and forces any buffered output bytes
  * to be written out. The general contract of <code>flush</code> is
  * that calling it is an indication that, if any bytes previously
  * written have been buffered by the implementation of the output
  * stream, such bytes should immediately be written to their
  * intended destination.
  * <p>
  * If the intended destination of this stream is an abstraction provided by
  * the underlying operating system, for example a file, then flushing the
  * stream guarantees only that bytes previously written to the stream are
  * passed to the operating system for writing; it does not guarantee that
  * they are actually written to a physical device such as a disk drive.
  * <p>
  * The <code>flush</code> method of <code>OutputStream</code> does nothing.
  *
  * @exception  IOException  if an I/O error occurs.
  */
  void OutputStream::flush()  {
 }

 /**
  * Closes this output stream and releases any system resources
  * associated with this stream. The general contract of <code>close</code>
  * is that it closes the output stream. A closed stream cannot perform
  * output operations and cannot be reopened.
  * <p>
  * The <code>close</code> method of <code>OutputStream</code> does nothing.
  *
  * @exception  IOException  if an I/O error occurs.
  */
void OutputStream::close() {
 }