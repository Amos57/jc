#pragma once

#include "Throwable.h"
#include "String.h"

#define SIZED    0x00000040
#define SUBSIZED 0x00004000
#define ORDERED  0x00000010

import java::lang::String;
import java::lang::IllegalArgumentException;
import java::lang::IndexOutOfBoundsException;

 namespace java {
	 namespace nio {
	 

abstract class Buffer extends java::lang::Object{
protected:
	static const int SPLITERATOR_CHARACTERISTICS;

protected:
	int mark_ = -1;
	int position_ =0;
	int limit_;
	int capact;
	// Used only by direct buffers
    // NOTE: hoisted here for speed in JNI GetDirectBufferAddress
	long address;

protected:
	// Creates a new buffer with the given mark, position, limit, and capacity,
    // after checking invariants.
    //
	Buffer(const int& mark,const int& pos,const int& lim,const int& cap) :
	address(0),
	limit_(0),
	capact(0)
	{       // package-private
		if (cap < 0)
			throw  IllegalArgumentException("Negative capacity: " + cap);
		this->capact = cap;
		limit(lim);
		position(pos);
		if (mark_ >= 0) {
			if (mark_ > pos)
				throw  1;// IllegalArgumentException("mark > position: (" + mark + String(" > ") + String::valueOf(pos) + ")");
			this->mark_ = mark;
		}
	}
	/**
	* Returns this buffer's capacity.
	*
	* @return  The capacity of this buffer
	*/
public : int capacity() {
		return capact;
	}

	/**
	 * Returns this buffer's position.
	 *
	 * @return  The position of this buffer
	 */
  int position() {
		return position_;
	}

  final Buffer& position(int newPosition) {
	  if ((newPosition > limit_) || (newPosition < 0))
		  throw new IllegalArgumentException();
	  position_ = newPosition;
	  if (mark_ > position_) mark_ = -1;
	  return *this;
  }

	/**
	 * Sets this buffer's position.  If the mark is defined and larger than the
	 * new position then it is discarded.
	 *
	 * @param  newPosition
	 *         The new position value; must be non-negative
	 *         and no larger than the current limit
	 *
	 * @return  This buffer
	 *
	 * @throws  IllegalArgumentException
	 *          If the preconditions on <tt>newPosition</tt> do not hold
	 */
     Buffer& positionf(int newPosition) {
		if ((newPosition > limit_) || (newPosition < 0))
			throw new IllegalArgumentException();
		position_ = newPosition;
		if (mark_ > position_) mark_ = -1;
		return *this;
	}

	/**
	 * Returns this buffer's limit.
	 *
	 * @return  The limit of this buffer
	 */
public: int limit() {
		return limit_;
	}

	/**
	 * Sets this buffer's limit.  If the position is larger than the new limit
	 * then it is set to the new limit.  If the mark is defined and larger than
	 * the new limit then it is discarded.
	 *
	 * @param  newLimit
	 *         The new limit value; must be non-negative
	 *         and no larger than this buffer's capacity
	 *
	 * @return  This buffer
	 *
	 * @throws  IllegalArgumentException
	 *          If the preconditions on <tt>newLimit</tt> do not hold
	 */
public : Buffer& limit(int newLimit) {
		if ((newLimit > capact) || (newLimit < 0))
			throw new IllegalArgumentException();
		limit_ = newLimit;
		if (position_ > limit_) position_ = limit_;
		if (mark_ > limit_) mark_ = -1;
		return *this;
	}

	/**
	 * Sets this buffer's mark at its position.
	 *
	 * @return  This buffer
	 */
	public:  Buffer& mark() {
		mark_ = position_;
		return *this;
	}

	/**
	 * Resets this buffer's position to the previously-marked position.
	 *
	 * <p> Invoking this method neither changes nor discards the mark's
	 * value. </p>
	 *
	 * @return  This buffer
	 *
	 * @throws  InvalidMarkException
	 *          If the mark has not been set
	 */
	public : Buffer& reset() {
		int m = mark_;
		if (m < 0)
			throw 1;// new InvalidMarkException();
		position_ = m;
		return *this;
	}

	/**
	 * Clears this buffer.  The position is set to zero, the limit is set to
	 * the capacity, and the mark is discarded.
	 *
	 * <p> Invoke this method before using a sequence of channel-read or
	 * <i>put</i> operations to fill this buffer.  For example:
	 *
	 * <blockquote><pre>
	 * buf.clear();     // Prepare buffer for reading
	 * in.read(buf);    // Read data</pre></blockquote>
	 *
	 * <p> This method does not actually erase the data in the buffer, but it
	 * is named as if it did because it will most often be used in situations
	 * in which that might as well be the case. </p>
	 *
	 * @return  This buffer
	 */
	public : Buffer& clear() {
		position_ = 0;
		limit_ = capact;
		mark_ = -1;
		return *this;
	}

	/**
	 * Flips this buffer.  The limit is set to the current position and then
	 * the position is set to zero.  If the mark is defined then it is
	 * discarded.
	 *
	 * <p> After a sequence of channel-read or <i>put</i> operations, invoke
	 * this method to prepare for a sequence of channel-write or relative
	 * <i>get</i> operations.  For example:
	 *
	 * <blockquote><pre>
	 * buf.put(magic);    // Prepend header
	 * in.read(buf);      // Read data into rest of buffer
	 * buf.flip();        // Flip buffer
	 * out.write(buf);    // Write header + data to channel</pre></blockquote>
	 *
	 * <p> This method is often used in conjunction with the {@link
	 * java.nio.ByteBuffer#compact compact} method when transferring data from
	 * one place to another.  </p>
	 *
	 * @return  This buffer
	 */
	public:  Buffer& flip() {
		limit_ = position_;
		position_ = 0;
		mark_ = -1;
		return *this;
	}

	/**
	 * Rewinds this buffer.  The position is set to zero and the mark is
	 * discarded.
	 *
	 * <p> Invoke this method before a sequence of channel-write or <i>get</i>
	 * operations, assuming that the limit has already been set
	 * appropriately.  For example:
	 *
	 * <blockquote><pre>
	 * out.write(buf);    // Write remaining data
	 * buf.rewind();      // Rewind buffer
	 * buf.get(array);    // Copy data into array</pre></blockquote>
	 *
	 * @return  This buffer
	 */
	public:  Buffer& rewind() {
		position_ = 0;
		mark_ = -1;
		return *this;
	}

	/**
	 * Returns the number of elements between the current position and the
	 * limit.
	 *
	 * @return  The number of elements remaining in this buffer
	 */
	public:  int remaining() const{
		return limit_ - position_;
	}

	/**
	 * Tells whether there are any elements between the current position and
	 * the limit.
	 *
	 * @return  <tt>true</tt> if, and only if, there is at least one element
	 *          remaining in this buffer
	 */
	public:  bool hasRemaining() const{
		return position_ < limit_;
	}

	/**
	 * Tells whether or not this buffer is read-only.
	 *
	 * @return  <tt>true</tt> if, and only if, this buffer is read-only
	 */
	public:virtual abstract bool isReadOnly()=0;

	/**
	 * Tells whether or not this buffer is backed by an accessible
	 * array.
	 *
	 * <p> If this method returns <tt>true</tt> then the {@link #array() array}
	 * and {@link #arrayOffset() arrayOffset} methods may safely be invoked.
	 * </p>
	 *
	 * @return  <tt>true</tt> if, and only if, this buffer
	 *          is backed by an array and is not read-only
	 *
	 * @since 1.6
	 */
	public:virtual abstract bool hasArray()=0;

	/**
	 * Returns the array that backs this
	 * buffer&nbsp;&nbsp;<i>(optional operation)</i>.
	 *
	 * <p> This method is intended to allow array-backed buffers to be
	 * passed to native code more efficiently. Concrete subclasses
	 * provide more strongly-typed return values for this method.
	 *
	 * <p> Modifications to this buffer's content will cause the returned
	 * array's content to be modified, and vice versa.
	 *
	 * <p> Invoke the {@link #hasArray hasArray} method before invoking this
	 * method in order to ensure that this buffer has an accessible backing
	 * array.  </p>
	 *
	 * @return  The array that backs this buffer
	 *
	 * @throws  ReadOnlyBufferException
	 *          If this buffer is backed by an array but is read-only
	 *
	 * @throws  UnsupportedOperationException
	 *          If this buffer is not backed by an accessible array
	 *
	 * @since 1.6
	 */
	public:virtual abstract jchar* array()=0;

	/**
	 * Returns the offset within this buffer's backing array of the first
	 * element of the buffer&nbsp;&nbsp;<i>(optional operation)</i>.
	 *
	 * <p> If this buffer is backed by an array then buffer position <i>p</i>
	 * corresponds to array index <i>p</i>&nbsp;+&nbsp;<tt>arrayOffset()</tt>.
	 *
	 * <p> Invoke the {@link #hasArray hasArray} method before invoking this
	 * method in order to ensure that this buffer has an accessible backing
	 * array.  </p>
	 *
	 * @return  The offset within this buffer's array
	 *          of the first element of the buffer
	 *
	 * @throws  ReadOnlyBufferException
	 *          If this buffer is backed by an array but is read-only
	 *
	 * @throws  UnsupportedOperationException
	 *          If this buffer is not backed by an accessible array
	 *
	 * @since 1.6
	 */
	public:virtual abstract int arrayOffset()=0;

	/**
	 * Tells whether or not this buffer is
	 * <a href="ByteBuffer.html#direct"><i>direct</i></a>.
	 *
	 * @return  <tt>true</tt> if, and only if, this buffer is direct
	 *
	 * @since 1.6
	 */
	public: virtual abstract bool isDirect() =0;


	// -- Package-private methods for bounds checking, etc. --

	/**
	 * Checks the current position against the limit, throwing a {@link
	 * BufferUnderflowException} if it is not smaller than the limit, and then
	 * increments the position.
	 *
	 * @return  The current position value, before it is incremented
	 */
	final int nextGetIndex() {                          // package-private
		if (position_ >= limit_)
			throw 1;// BufferUnderflowException();
		return position_++;
	}

	final int nextGetIndex(int nb) {                    // package-private
		if (limit_ - position_ < nb)
			throw 1;//  BufferUnderflowException();
		int p = position_;
		position_ += nb;
		return p;
	}

	/**
	 * Checks the current position against the limit, throwing a {@link
	 * BufferOverflowException} if it is not smaller than the limit, and then
	 * increments the position.
	 *
	 * @return  The current position value, before it is incremented
	 */
	final int nextPutIndex() {                          // package-private
		if (position_ >= limit_)
			throw 1;// BufferOverflowException();
		return position_++;
	}

	 int nextPutIndex(const int& nb) {                    // package-private
		if (limit_ - position_ < nb)
			throw 1;// BufferOverflowException();
		int p = position_;
		position_ += nb;
		return p;
	}

	/**
	 * Checks the given index against the limit, throwing an {@link
	 * IndexOutOfBoundsException} if it is not smaller than the limit
	 * or is smaller than zero.
	 */
	final int checkIndex(const int& i) {                       // package-private
		if ((i < 0) || (i >= limit_))
			throw  IndexOutOfBoundsException();
		return i;
	}

	final int checkIndex(const int& i,const int& nb) {               // package-private
		if ((i < 0) || (nb > limit_ - i))
			throw  IndexOutOfBoundsException();
		return i;
	}

	final int markValue() {                             // package-private
		return mark_;
	}

	final void truncate() {                             // package-private
		mark_ = -1;
		position_ = 0;
		limit_ = 0;
		capact = 0;
	}

	final void discardMark() {                          // package-private
		mark_ = -1;
	}

	static void checkBounds(const int& off,const int& len,const int& size) { // package-private
		if ((off | len | (off + len) | (size - (off + len))) < 0)
			throw  IndexOutOfBoundsException();
	}


};
	 }
 }
