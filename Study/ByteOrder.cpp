#include "ByteOrder.h"

import java::nio::ByteOrder;

final ByteOrder ByteOrder::BIG_ENDIAN = ByteOrder("BIG_ENDIAN");
final ByteOrder ByteOrder::LITTLE_ENDIAN = ByteOrder("LITTLE_ENDIAN");