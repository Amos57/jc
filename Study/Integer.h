#pragma once

#include "String.h"

namespace java {
	namespace lang {
class Integer
{
private:
	int value;

public:
	static const int MAX_VALUE;
	static const int MIN_VALUE;
	static const char digits[36];
	static const char DigitOnes[];
	static const char DigitTens[];
	Integer();
	Integer(const int & num);



	Integer& operator=(const int& init);
	Integer& operator=(const Integer& that);
	Integer& operator+=(const Integer& that);
	Integer& operator/=(const Integer& that);
	Integer& operator*=(const Integer& that);
	Integer& operator%=(const Integer& that);
	Integer& operator-=(const Integer& that);
	Integer& operator+(const Integer& that);
	Integer& operator--();
	Integer& operator++();
	operator int() const;
	bool operator!=(const Integer& that);

	friend  std::ostream& operator<< (std::ostream& os, const Integer& that);
	String toString();
	static String toString(const int& val);
	static  void getChars(int& i, int& index, char* buf);
	int intValue();
	static int parseInt(const String& s);

	static String toString( int val,  int radix);
	static String toHexString(const int & val);
};


}
}