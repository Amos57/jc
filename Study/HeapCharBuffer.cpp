
#include "CharBuffer.cpp"
#include "System.h"

import java::nio::HeapCharBuffer;
import java::nio::ByteOrder;
import java::nio::CharBuffer;
import java::lang::System;

HeapCharBuffer::HeapCharBuffer(const int& one, const int& two)
	: CharBuffer(-1, 0, limit_, one, new jchar[one], 0) {}

HeapCharBuffer::HeapCharBuffer(jchar* buf, const int& size, int off, int len) :
	CharBuffer(-1, off, off + len, size, buf, 0) {}// package-private

HeapCharBuffer::HeapCharBuffer(jchar* buf,
	const int& mark, const int& pos, const int& lim, const int& cap,
	const int& off) : CharBuffer(mark, pos, lim, cap, buf, off)
{}

const CharBuffer& HeapCharBuffer::slice() {
	return  static_cast<const CharBuffer&>(HeapCharBuffer(&hb[0], -1, 0, this->remaining(), this->remaining(), this->position() + offset));
}

const CharSequence& HeapCharBuffer::subSequence(const int& start, const int& end) {
	if ((start < 0)
		|| (end > length())
		|| (start > end))
		throw  IndexOutOfBoundsException();
	int pos = position();
	return  static_cast<const CharSequence&>(HeapCharBuffer(&hb[0], -1, pos + start, pos + end, capacity(), offset));
}

CharBuffer& HeapCharBuffer::asReadOnlyBuffer() {
	return *this;
	//return new HeapCharBufferR(hb,
	//	this->markValue(),
	//	this->position(),
	//	this->limit(),
	//	this->capacity(),
	//	offset);
}

 CharBuffer& HeapCharBuffer::put(CharBuffer& src) {
	
	//if (sb = dynamic_cast<HeapCharBuffer*>(&src)) {
		
     HeapCharBuffer* sb=static_cast<HeapCharBuffer*>(&src);
    if (&src == this){
			throw new IllegalArgumentException();

		int n = sb->remaining();
		if (n > remaining())
			throw 1;// new BufferOverflowException();
		System::arraycopy(&sb->hb[0], sb->ix(sb->position()),&hb[0], ix(position()), n);
			
		sb->position(sb->position() + n);
		position(position() + n);
	}
	else if (src.isDirect()) {
		int n = src.remaining();
		if (n > remaining())
			throw 1;// new BufferOverflowException();
		src.get(hb, ix(position()), n);
		position(position() + n);
	}
	else {
		CharBuffer::put(src);
	}
	return *this;
	
}


 CharBuffer& HeapCharBuffer::duplicate() {
 
	 return *this;
 }