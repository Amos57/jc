#pragma once
#include "String.h"
#include <vector>
#include <windows.h>


namespace java {
	namespace io {
		using  java::lang::Object;
		using  java::lang::String;



		//class i  :public int{};

		class File extends Object
		{

		private:const String path;
			    enum PathStatus { NULL_PTR=-1, INVALID, CHECKED };
				const int prefixLength;
				int getPrefixLength();
				bool isInvalid();
				PathStatus status = PathStatus::NULL_PTR;
				WIN32_FIND_DATA data;
				LPCWSTR warPath;
				String name;
				bool exst;

		public:
			static const char separatorChar;
			static const String separator;
			static const char pathSeparatorChar;
			static const String pathSeparator;


		 File(const String& path,const int& prefixLength);
		 File(const String& path);
		 File(const File&);
		 ~File();


		 String getName();
		 String getParent();
		 File getParentFile();
		 String getPath();
		 String getAbsolutePath();
		 bool canRead();
		 bool canWrite();
		 bool exists();
		 bool isDirectory();
		 bool isFile();
		 bool isHidden();
		 unsigned long lastModified();
	     long length();
		 bool remove();
		 void deleteOnExit();
		 bool mkdir();
		 bool mkdirs();
		 bool createNewFile();


		 std::vector<String> list();
		 std::vector<File> listFiles();
		 std::vector<File> listFiles(bool (*accepy)(const File&));





		};

	}
}

