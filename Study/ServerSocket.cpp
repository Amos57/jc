#include "ServerSocket.h"
#include "Integer.h"

import java::net::ServerSocket;
import java::net::Socket;
import java::io::IOException;
import java::lang::Integer;


ServerSocket::ServerSocket(const int & port) {
	WSAData wdata;
	WORD dll = MAKEWORD(2, 1);
	if (WSAStartup(dll, &wdata) != 0) {
		throw IOException("fail load library");
	}

	sizeStruct = sizeof(sa);
	sa.sin_port = port;
	sa.sin_addr.s_addr = inet_addr("127.0.0.1");
	sa.sin_family = AF_INET;
	sock = socket(AF_INET, SOCK_STREAM, NULL);

	bind(sock, (SOCKADDR*)&sa, sizeStruct);

	listen(sock, SOMAXCONN);
}
	Socket  ServerSocket::accept_() {
		SOCKET newConnect = accept(sock, (SOCKADDR*)&sa, &sizeStruct);
		
		if (newConnect == INVALID_SOCKET) {
			throw IOException(String("last code of error: ")+WSAGetLastError());
		}
		Socket s;
		s.socke = newConnect;
		s.closing = false;
		s.isConnect = true;
		return s;
}
