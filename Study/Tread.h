#pragma once

#include "Runnable.h"
#include "String.h"


namespace java {
	namespace lang {
class Thread implements Runnable
{

private:
	Runnable& target;

public:
	Thread();
	Thread(Runnable& r);
	Thread(Runnable& r,const String & name);
	
	
      void run() {
			if (&target != nullptr) {
				target.run();
			}
		}


	static void sleep(__int64 mls);
	static Thread& getCurrentThread();

};
}
}