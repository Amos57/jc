#pragma once
#include "IOStreamAbst.h"

namespace java {
	namespace io {

		class ByteArrayInputStream : public InputStream
		{
		protected: ByteArray *buf = nullptr;
				 int pos;
				 int mark_;
				 int count;
		public:
			     ByteArrayInputStream(ByteArray& buf);
				 ByteArrayInputStream(ByteArray& buf, const int& off, const int& len);
				 int read();
				 int read(ByteArray& buf,  int off,  int len);
				 long skip(const long &n);
				 int available();
				 bool markSupported();
				 void mark(const int &readAheadLimit);
				 void reset();
				 void close();

		};

	}

}