#pragma once
#include <iostream>
#include "String.h"

namespace java {
	namespace lang {
		class Throwable extends std::exception , public Object {

		public:
			Throwable() throw() : std::exception() {}
			Throwable(char const* const message) throw()
				: std::exception(message) {}


			virtual char const* getMessage() const throw()
			{
				return std::exception::what();
			}

		};

		class Exception extends Throwable {

		public:
			Exception() throw()
				: Throwable() {}
			Exception( const String& message) throw()
				: Throwable(message.toStdString().c_str()) {}
			Exception(char const* const message) throw()
				: Throwable(message) {}


		};
		
		class NullPointerException extends Exception {
		public:
			NullPointerException() throw()
				: Exception() {}
			NullPointerException(const String& message) throw()
				: Exception(message.toStdString().c_str()) {}
			NullPointerException(char const* const message) throw()
				: Exception(message) {}

		};

		class IndexOutOfBoundsException extends Exception {
		public:
			IndexOutOfBoundsException() throw()
				: Exception() {}
			IndexOutOfBoundsException(const String& message) throw()
				: Exception(message.toStdString().c_str()) {}
			IndexOutOfBoundsException(char const* const message) throw()
				: Exception(message) {}

		};

		class IllegalArgumentException extends Exception {
		public:
			IllegalArgumentException() throw()
				: Exception() {}
			IllegalArgumentException(const String& message) throw()
				: Exception(message.toStdString().c_str()) {}
			IllegalArgumentException(char const* const message) throw()
				: Exception(message) {}

		};

		
	}
	namespace io {
		using lang::Exception;

		class IOException : public Exception{

		public:
			IOException() throw(): Exception() {};
				
			IOException(const java::lang::String& message) throw(): Exception(message.toStdString().c_str()) {};
				
			IOException(char const* const message) throw(): Exception(message) {};
				


		};

		class FileNotFoundException : public IOException {
		public:
			FileNotFoundException() throw(): IOException() {}
				
			FileNotFoundException(const java::lang::String& message) throw(): IOException(message) {};
				
			FileNotFoundException(char const* const message) throw(): IOException(message) {}
				


		};
	
	}
}

