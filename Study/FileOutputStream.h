#pragma once
#include "IOStreamAbst.h"
#include "FileDescriptor.h"

import java::lang::String;

namespace java{
	namespace io {
		class FileOutputStream extends OutputStream
		{
		private: FileDescriptor fd;
			     HANDLE deskr = 0;
		public: FileOutputStream(const String& path);
			    FileOutputStream(const String& path,const bool& append);
				FileOutputStream(const FileDescriptor& fdObj);
				void write(const int& b);
				void write(const ByteArray& buf);
				void write(const ByteArray& buf,const int& offset,const int& len);
				void close();
				FileDescriptor getFD() const;

				~FileOutputStream();

		};
	}
}