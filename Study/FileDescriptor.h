#pragma once
#include <Windows.h>



namespace java {
	namespace io {



		class FileDescriptor
		{
			
		private:
			FileDescriptor(const int& fd);
			
			int fd;
            static HANDLE set(const int& val);
            static FileDescriptor standardStream(int fd);

		public:
			HANDLE handle = 0;
			FileDescriptor();
			bool valid();
			const static FileDescriptor in;
			const static FileDescriptor err;
			const static FileDescriptor out;

			
		};

	};

};