#include "File.h"
#include <shlwapi.h>
#include <iostream>

#pragma comment(lib, "Shlwapi.lib")
#pragma warning(3:4996)

using namespace java::io;
using namespace java::lang;

const char File::separatorChar = '/';
const String File::separator   = "/";

const char File::pathSeparatorChar = ':';
const String File::pathSeparator   = ":";


File::File(const File& init) :name(init.name),path(init.path), prefixLength(init.prefixLength), exst(init.exst){

	jchar* warPath = new jchar[path.length() + 1];
	this->warPath = warPath;
	wmemcpy(warPath, path.toWString().c_str(), path.length());
	warPath[path.length()] = L'\0';
	HANDLE hNextFind = FindFirstFile(warPath, &data);
	FindClose(hNextFind);
}



File::File(const String & path): path(path), prefixLength(0),name(""){

	jchar *warPath = new jchar[path.length()+1];
	this->warPath = warPath;
	wmemcpy(warPath, path.toWString().c_str(),path.length());
	warPath[path.length()] = L'\0';

	HANDLE hNextFind;
	hNextFind= FindFirstFile(warPath,&data);
	exst = hNextFind != INVALID_HANDLE_VALUE;
	if(exst)
	    name = data.cFileName;
	else {
		std::vector<String> folders = String((const jchar*)warPath).replaceAll("\\","/").split('/');
		String n = folders[folders.size() - 1];
		if (n.equals("")) {
		     n = folders[folders.size() - 2];
		}
		name = n;
	}
		FindClose(hNextFind);
}



bool File::isInvalid() {
	if (status == PathStatus::NULL_PTR) {
		status = (path.indexOf('\u0000') < 0) ? PathStatus::CHECKED
			: PathStatus::INVALID;
	}
	return status == PathStatus::INVALID;
}



bool File::isDirectory() {
	return data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY;
}

bool File::isFile() {
	return data.dwFileAttributes & FILE_ATTRIBUTE_NORMAL;
}

bool File::isHidden() {
	return data.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN;
}

bool File::canRead() {
	return data.dwFileAttributes & FILE_ATTRIBUTE_READONLY;
}

bool File::canWrite() {
	return true;
}

bool File::remove() {
	return isDirectory() ?  !RemoveDirectory(warPath) : !DeleteFile(warPath);
}

String File::getName()
{
	return name;
}

unsigned long File::lastModified() {
 return	data.ftLastWriteTime.dwLowDateTime;
}

long File::length() {
	return data.nFileSizeLow;
}

String File::getAbsolutePath() {

 if (!exists()) {
 	std::string t = String(warPath).toStdString();
 	if (!PathIsRelativeA(t.c_str())) {
 		return t.c_str();
 	}
 	jchar dir[100];
 	GetCurrentDirectory(100,dir);
 	return String(dir) + String(warPath);
	}else {
		jchar init[100];
		GetFullPathName(warPath, 100, init, NULL);
		return init;
	}
}

bool File::mkdir() {
	BOOL temp = CreateDirectory(warPath, NULL);
	HANDLE h;
	if (temp) {
		h= FindFirstFile(warPath, &data);
		exst = h != INVALID_HANDLE_VALUE;
        FindClose(h);
	}
	return temp;
}

bool File::mkdirs() {
	if (exists()) {
		return false;
	}
	if (mkdir()) {
		return true;
	}
	std::vector<String> folders = getAbsolutePath().split('\\');
    String temp = folders[0];
	for (int i=1;i< folders.size();i++) {
		temp += String("/") + folders[i];
		File tempFile(temp);
		if (tempFile.exists())
			continue;
		else {
			if (!tempFile.mkdir())
				return false;
		}
	   
	}
	return true;
}
bool File::exists() {
	return exst;
}

bool File::createNewFile() {
	HANDLE hNextFind;
	HANDLE temp=CreateFile(warPath, GENERIC_WRITE, 0, NULL,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (temp)
		CloseHandle(temp);
	hNextFind = FindFirstFile(warPath, &data);
	exst = hNextFind != INVALID_HANDLE_VALUE;
	return exst;
}


String File::getParent() {
	String p = getAbsolutePath();
	return p.substring(0,p.lastIndexOf(getName())-1);
}

File File::getParentFile() {
	String path = getParent();
	return File(path);
}

std::vector<String> File::list() {
	std::vector<String> array{};
	if (!isDirectory())
		return array;

	if (!exists())
		return array;


	std::wstring s(warPath);
	s += L'\\';
	s += L'*';

	HANDLE files;
    WIN32_FIND_DATA f;
	LPCWSTR p= s.c_str();
	files = FindFirstFile(p,&f);
	do {

		array.push_back(f.cFileName);

	}while(FindNextFile(files, &f));
	if (files)
		FindClose(files);
	return array;
}

std::vector<File> File::listFiles() {
	std::vector<File> array{};
	if (!isDirectory())
		return array;

	if (!exists())
		return array;


	std::wstring s(warPath);
	s += L'\\';
	s += L'*';


	WIN32_FIND_DATA f;
	LPCWSTR p = s.c_str();
	const HANDLE files = FindFirstFile(p, &f);
	do {

		String path = getAbsolutePath() + "\\" + f.cFileName;
		if (path.endWith("..") || path.endWith("."))
			continue;
		File file(getAbsolutePath()+"\\"+f.cFileName);
		array.push_back(file);
	} while (FindNextFile(files, &f));

		FindClose(files);
	return array;
}
std::vector<File> File::listFiles(bool (*accept)(const File& fil)) {
	std::vector<File> array{};
	if (!isDirectory())
		return array;

	if (!exists())
		return array;


	std::wstring s(warPath);
	s += L'\\';
	s += L'*';


	WIN32_FIND_DATA f;
	LPCWSTR p = s.c_str();
	const HANDLE files = FindFirstFile(p, &f);
	do {

		String path = getAbsolutePath() + "\\" + f.cFileName;
		if (path.endWith("..") || path.endWith("."))
			continue;
		File file(getAbsolutePath() + "\\" + f.cFileName);
		if(accept(file))
		array.push_back(file);
	} while (FindNextFile(files, &f));

	FindClose(files);
	return array;
}



String File::getPath() {
	return path;
}

File::~File() {
	if (warPath)
		delete[] warPath;
}