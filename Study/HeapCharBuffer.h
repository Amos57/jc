#pragma once

#include "CharBuffer.h"

namespace java {
	namespace nio {

		class HeapCharBuffer :public CharBuffer
		{
		public:
			HeapCharBuffer(const int& one, const int& two);


			HeapCharBuffer(jchar* buf, const int& size, int off, int len);
              
				

		protected: HeapCharBuffer(jchar* buf,
			const int& mark, const int& pos, const int& lim, const int& cap,
			const int& off);


const CharBuffer& slice();

CharBuffer& duplicate();

CharBuffer& asReadOnlyBuffer();



int ix(const int& i) {
	return i + offset;
}

jchar get() {
	return hb[ix(nextGetIndex())];
}

jchar get(const int& i) {
	return hb[ix(checkIndex(i))];
}


jchar getUnchecked(const int& i) {
	return hb[ix(i)];
}


CharBuffer& get(jchar* dst,const int& offset,const int& length);

bool isDirect() {
	return false;
}

 bool isReadOnly() {
	return false;
}

CharBuffer& put(const jchar& x) {

	hb[ix(nextPutIndex())] = x;
	return *this;

}

CharBuffer& put(const int& i,const jchar& x);

CharBuffer& put(jchar* src,const int& offset,const int& length);

CharBuffer& put(CharBuffer& src);

CharBuffer& compact();



std::string toString(int start, int end);


			// --- Methods to support CharSequence ---

const CharSequence& subSequence(const int& start, const int& end);

std::string toString(const int& start,const int& end) {               // package-private
	try {
		return  String(&hb[0], start + offset, end - start).toStdString();
	}
	catch (...) {
		throw  IndexOutOfBoundsException();
	}

}


ByteOrder order() {
	return ByteOrder::nativeOrder();
}



		};

	}
}

