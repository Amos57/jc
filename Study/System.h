#pragma once

#include "IOStreamAbst.h"
#include "PrintStream.h"
#include "Object.h"
#include <windows.h>

using  java::io::InputStream;
using  java::io::PrintStream;
namespace java {
	namespace lang {
		class System : public Object
		{
		public:  static InputStream *in;
			     static PrintStream out;

		public:
			static long long currentTimeMillis()
			{
				static const __int64 magic = 116444736000000000; // 1970/1/1
				SYSTEMTIME st;
				GetSystemTime(&st);
				FILETIME   ft;
				SystemTimeToFileTime(&st, &ft); // in 100-nanosecs...
				
				long long t;
				memcpy(&t, &ft, sizeof( t));
				return (t - magic) / 10000; // scale to millis.
			}


			static  void arraycopy(const Object* src, int  srcPos,
				 Object* dest, int destPos,
				int length) {
				memcpy(&dest[destPos],&src[srcPos],length);
			}

			static  void arraycopy(const INT32* src, int  srcPos,
				INT32* dest, int destPos,
				int length) {
				memcpy(&dest[destPos], &src[srcPos], length);
			}

			static  void arraycopy(const char* src, int  srcPos,
				char* dest, int destPos,
				int length) {
				memcpy(&dest[destPos], &src[srcPos], length);
			}

			static  void arraycopy(const wchar_t* src, int  srcPos,
				wchar_t* dest, int destPos,
				int length) {
				memcpy(&dest[destPos], &src[srcPos], length);
			}

			static void exit(int status) {
				exit(status);
			}

		};

	}

}

