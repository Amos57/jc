#include "Socket.h"
#include "Throwable.h"

import java::net::Socket;
import java::io::IOException;


Socket::Socket()  : 
	socke(0),
	isConnect(false),
	closing(false),
	sockOutpStraem(&socke, &isConnect, &closing),
	sockInputStream(&socke, &isConnect,&closing)
	
{}
Socket::Socket(const String& host, const int& port) :
	socke(0),
	isConnect(false),
	closing(false),
	sockOutpStraem(&socke, &isConnect, &closing),
	sockInputStream(&socke, &isConnect, &closing)
{
	WSAData wdata;
	WORD dll = MAKEWORD(2,1);
	if (WSAStartup(dll, &wdata)!=0) {
		throw IOException("fail connected");
	}

	SOCKADDR_IN sa;
	int structAddr = sizeof(sa);
	sa.sin_port = port;
	sa.sin_addr.s_addr = inet_addr(host.toStdString().c_str());
	sa.sin_family = AF_INET;
	socke = socket(AF_INET,SOCK_STREAM,NULL);

	if (connect(socke, (SOCKADDR*)&sa, structAddr)!=0) {
		throw IOException("fail connected");
	}
	isConnect = true;
	closing = false;
}

Socket::SocketInputStream java::net::Socket::getInputStream()
{
	return sockInputStream;
}

Socket::SocketOutputStraem java::net::Socket::getOutputStream()
{
	return sockOutpStraem;
}
void Socket::close() {
	if (!closing)
		return;

	closesocket(socke);
	closing = true;
	isConnect = false;
	
}

bool Socket::isClosed() {
	return closing;
	
}

bool Socket::isConnected() {
	return isConnect;
}

Socket::~Socket() {}